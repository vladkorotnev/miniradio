//
//  MRHeadsetButtonControl.m
//  MiniRadio
//
//  Created by Akasaka Ryuunosuke on 05/11/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import "MRHeadsetButtonControl.h"
#import "MRTuner.h"
@implementation MRHeadsetButtonControl
+ (MRHeadsetButtonControl *)sharedInstance
{
    static MRHeadsetButtonControl *sharedInstance = nil;
    if (sharedInstance == nil)
    {
        sharedInstance = [[self alloc] init];
    }
    return sharedInstance;
}
- (MRHeadsetButtonControl*) init {
    self = [super init];
    if (self) {
        NSLog(@"HPCTL init");
        [[NSNotificationCenter defaultCenter]
         addObserver:self
         selector:@selector(_volumeChanged:)
         name:@"AVSystemController_SystemVolumeDidChangeNotification"
         object:nil];
        _lastVolChange = [NSDate date];
        _volview = [[MPVolumeView alloc]init];
        for (UIView*sas in _volview.subviews) {
            if ([sas isKindOfClass:[UISlider class]]) {
                 _prevVol = ((UISlider*)sas).value;
                NSLog(@"Init volume %f",_prevVol);
            }
        }
    }
    return self;
}
- (void)remoteControlReceivedWithEvent:(UIEvent *)receivedEvent
{
    if (receivedEvent.type != UIEventTypeRemoteControl) return;
    if ([self _isHeadsetPluggedIn]) {
        NSString*k = @"hp-";
            switch (receivedEvent.subtype) {
                case UIEventSubtypeRemoteControlPause: /* FALLTHROUGH */
                case UIEventSubtypeRemoteControlPlay:  /* FALLTHROUGH */
                case UIEventSubtypeRemoteControlTogglePlayPause:
                    k = [k stringByAppendingString:@"one"];
                    break;
                case UIEventSubtypeRemoteControlNextTrack:
                    k = [k stringByAppendingString:@"two"];
                    break;
                case UIEventSubtypeRemoteControlPreviousTrack:
                    k = [k stringByAppendingString:@"three"];
                    break;
                case UIEventSubtypeRemoteControlBeginSeekingForward:
                    k = [k stringByAppendingString:@"twohold"];
                    break;
                case UIEventSubtypeRemoteControlBeginSeekingBackward:
                    k = [k stringByAppendingString:@"threehold"];
                    break;
                default:
                    return;
                    break;
            }
        NSLog(@"HPCTL event: %@ = %@", k,[[NSUserDefaults standardUserDefaults]objectForKey:k]);
        NSString*action = [[NSUserDefaults standardUserDefaults]objectForKey:k];
        if (action) {
            [self _performActionEncodedByName:action];
        }
    } else {
        // No headset, act normally!
        switch (receivedEvent.subtype) {
            case UIEventSubtypeRemoteControlPause: /* FALLTHROUGH */
            case UIEventSubtypeRemoteControlPlay:  /* FALLTHROUGH */
            case UIEventSubtypeRemoteControlTogglePlayPause:
                [self _performActionEncodedByName:@"pause"];
                break;
            
                // TODO?
           /* case UIEventSubtypeRemoteControlNextTrack:
                k = [k stringByAppendingString:@"two"];
                break;
            case UIEventSubtypeRemoteControlPreviousTrack:
                k = [k stringByAppendingString:@"three"];
                break;
            case UIEventSubtypeRemoteControlBeginSeekingForward:
                k = [k stringByAppendingString:@"twohold"];
                break;
            case UIEventSubtypeRemoteControlBeginSeekingBackward:
                k = [k stringByAppendingString:@"threehold"];
                break; */
                
                
            default:
                break;
        }
    }
}
- (BOOL)_isHeadsetPluggedIn {
    AVAudioSessionRouteDescription* route = [[AVAudioSession sharedInstance] currentRoute];
    for (AVAudioSessionPortDescription* desc in [route outputs]) {
        if ([[desc portType] isEqualToString:AVAudioSessionPortHeadphones])
            return YES;
    }
    return NO;
}
- (void)_volumeChanged:(NSNotification *)notification
{
    if(![self _isHeadsetPluggedIn]) return;
      float volume = [[[notification userInfo]objectForKey:@"AVSystemController_AudioVolumeNotificationParameter"]floatValue];
    NSLog(@"HPCTL volume orig:%f prev:%f now:%f ti:%f",_origVol,_prevVol,volume,[[NSDate date]timeIntervalSinceDate:_lastVolChange]);

  
    if ([[NSDate date]timeIntervalSinceDate:_lastVolChange] > 0.5) {
        // Vol change is timed out
        _origVol = _prevVol;
        _prevVol = volume;
        _lastVolChange = [NSDate date];
        _volChangeCount = 1;
        return;
    } else {
        if (_origVol == volume && _prevVol != volume && _volChangeCount <= 2) {
            NSString*action = @"none";
            if (_prevVol > _origVol) {
                // Plus minus
                NSLog(@"Detected plus-minus");
               action =  [[NSUserDefaults standardUserDefaults]objectForKey:@"hp-plusminus"];
                
            } else if (_prevVol < _origVol) {
                // Minus plus
               NSLog(@"Detected minus-plus");
                action = [[NSUserDefaults standardUserDefaults]objectForKey:@"hp-minusplus"];
            }
            [self _performActionEncodedByName:action];
            _lastVolChange = [NSDate dateWithTimeIntervalSinceNow:-2];
            _volChangeCount=0;
        }
        _origVol = _prevVol;
        _prevVol = volume;
        _volChangeCount++;
    }
    
}
- (void) _performActionEncodedByName: (NSString*)action {
    NSLog(@"HPCTL: Perform action %@",action);
    if ([action isEqualToString:@"kioku"])
        [[MRTuner sharedTuner] kiokuWrite];
    else if ([action isEqualToString:@"info"])
        [[MRTuner sharedTuner] speakInfo];
    else if ([action isEqualToString:@"pause"])
        [[MRTuner sharedTuner] _playPause];
    else if ([action isEqualToString:@"silenz"])
        [[MRTuner sharedTuner] mannerMode];
}

@end
