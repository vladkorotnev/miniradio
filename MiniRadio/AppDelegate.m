//
//  AppDelegate.m
//  MiniRadio
//
//  Created by Akasaka Ryuunosuke on 25/10/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import "AppDelegate.h"
#import "MRTuner.h"
@interface AppDelegate ()
            

@end

@implementation AppDelegate
            

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    if (![[NSUserDefaults standardUserDefaults]boolForKey:@"reseted"]) {
        [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"reseted"];
        [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"did-show-intro"];
        [[NSUserDefaults standardUserDefaults]setBool:TRUE forKey:@"kioku-sample"];
        [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"kioku-notify"];
        [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"append-station"];
        [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"track-announce"];
        [[NSUserDefaults standardUserDefaults]setInteger:15 forKey:@"kioku-length"];
        [[NSUserDefaults standardUserDefaults]setObject:@"pause" forKey:@"hp-one"];
        [[NSUserDefaults standardUserDefaults]setObject:@"kioku" forKey:@"hp-two"];
        [[NSUserDefaults standardUserDefaults]setObject:@"info" forKey:@"hp-three"];
        [[NSUserDefaults standardUserDefaults]setObject:@"none" forKey:@"hp-twohold"];
        [[NSUserDefaults standardUserDefaults]setObject:@"none" forKey:@"hp-threehold"];
        [[NSUserDefaults standardUserDefaults]setObject:@"none" forKey:@"hp-minusplus"];
        [[NSUserDefaults standardUserDefaults]setObject:@"none" forKey:@"hp-plusminus"];
        [[NSUserDefaults standardUserDefaults]setFloat:0.4 forKey:@"speech-muting"];
        [[NSUserDefaults standardUserDefaults]setFloat:0.4 forKey:@"qsilence-level"];
        [[NSUserDefaults standardUserDefaults]setInteger:5 forKey:@"qsilence-length"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }

       [MRHeadsetButtonControl sharedInstance]; // just to init
    [[UINavigationBar appearance]setBarStyle:UIBarStyleBlack];
    [[UINavigationBar appearance]setTintColor:[UIColor whiteColor]];
    [[UITabBar appearance]setBarStyle:UIBarStyleBlack];
    [[UITabBar appearance]setTintColor:[UIColor whiteColor]];

    return YES;
}
- (void)remoteControlReceivedWithEvent:(UIEvent *)receivedEvent
{
    [[MRHeadsetButtonControl sharedInstance]remoteControlReceivedWithEvent:receivedEvent];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
