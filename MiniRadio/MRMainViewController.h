//
//  MRMainViewController.h
//  MiniRadio
//
//  Created by Akasaka Ryuunosuke on 07/11/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MRMainViewController : UITabBarController

@end
