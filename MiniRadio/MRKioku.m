//
//  MRKioku.m
//  MiniRadio
//
//  Created by Akasaka Ryuunosuke on 03/11/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import "MRKioku.h"
#import "MRTuner.h"
@implementation MRKioku
+ (MRKioku *)sharedInstance
{
    static MRKioku *sharedInstance = nil;
    if (sharedInstance == nil)
    {
        sharedInstance = [[self alloc] init];
    }
    return sharedInstance;
}
- (MRKioku*)init {
    self = [super init];
    if (self) {
        kiokudata = [[[NSUserDefaults standardUserDefaults]objectForKey:@"kioku"] mutableCopy];
        if(!kiokudata) kiokudata = [NSMutableArray new];
    }
    return self;
}
- (void) addToKioku:(NSString*)title ofStation:(NSString*)station audioSampleFile:(NSString *)name{
    NSDictionary*o = @{@"title":title,@"station":station, @"file":name};
    [kiokudata addObject:o];
    [[NSUserDefaults standardUserDefaults]setObject:kiokudata forKey:@"kioku"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [[NSNotificationCenter defaultCenter]postNotificationName:kMRKiokuAddedNew object:o];
}
- (bool) isKiokuSampleAvailable: (NSInteger) index {
   
    return [[NSFileManager defaultManager]fileExistsAtPath:[self _kiokuSamplePathAtIndex:index]];
}
- (NSString*) _kiokuSamplePathAtIndex:(NSInteger)index {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths[0];
    NSString *f = [documentsDirectory stringByAppendingPathComponent:kiokudata[index][@"file"]];
    return f;
}

- (void) playKiokuSample: (NSInteger)index {
    if(![self isKiokuSampleAvailable:index])return;
    

    MRTuner*t = [MRTuner sharedTuner];
    if (t.currStation)
            [t.currStation.representingViewController moveArrow:(-1*ARROW_HEIGHT)];
    [t _playUrl:[NSURL fileURLWithPath:[self _kiokuSamplePathAtIndex:index]]];
   
}

- (NSArray*)readKioku {
    return kiokudata ;
}
- (void) deleteFromKioku:(NSInteger)index {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths[0];
    NSString *f = [documentsDirectory stringByAppendingPathComponent:kiokudata[index][@"file"]];
    NSURL *url = [NSURL fileURLWithPath:f];
    [[NSFileManager defaultManager]removeItemAtURL:url error:nil];
    [kiokudata removeObjectAtIndex:index];
    [[NSUserDefaults standardUserDefaults]setObject:kiokudata forKey:@"kioku"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [[NSNotificationCenter defaultCenter]postNotificationName:kMRKiokuRemoved object:[NSNumber numberWithInteger:index]];
}
@end
