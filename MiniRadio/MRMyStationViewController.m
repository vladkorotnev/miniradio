//
//  MRMyStationViewController.m
//  MiniRadio
//
//  Created by Akasaka Ryuunosuke on 25/10/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import "MRMyStationViewController.h"
#import "MRTuner.h"
@interface MRMyStationViewController ()
{
    NSMutableArray *stations;
}
@end

@implementation MRMyStationViewController
- (IBAction)addNew:(id)sender {
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Input station", @"Input dialog") message:NSLocalizedString(@"Please enter station data", @"Input dialog") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"Input dialog") otherButtonTitles:@"OK", nil];
    [av setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
    
    // Alert style customization
    [[av textFieldAtIndex:1] setSecureTextEntry:NO];
    [[av textFieldAtIndex:0] setPlaceholder:NSLocalizedString(@"Station NAME", @"Input dialog")];
    [[av textFieldAtIndex:1] setPlaceholder:NSLocalizedString(@"Station URL", @"Input dialog")];
    [av show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSString* name = ( [alertView textFieldAtIndex:0].text);
    NSString*url =( [alertView textFieldAtIndex:1].text);
    if ([[url stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""])
        return;
    if (!name || [name isEqualToString:@""])
        name = url;
    if(! ([url hasPrefix:@"http://"] || [url hasPrefix:@"https://"]) )
        url = [@"http://" stringByAppendingString:url];
    [stations addObject:@{@"name":name, @"url":url}];
    [self.tableView reloadData];
    [self _write];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"stations"]) {
        stations = [[[NSUserDefaults standardUserDefaults]objectForKey:@"stations"]mutableCopy];
    } else  {
        stations = [NSMutableArray new];
        [stations addObject:@{@"name": @"NyanServer", @"url":@"http://radio.nyan.pw/station/stream"}];
        [stations addObject:@{@"name": @"Provoda.ch", @"url":@"http://radio.nyan.pw/station/provodach"}];
        [stations addObject:@{@"name": @"Hardrave Japan", @"url":@"http://www.hardrave-radio.jp:8000/stream/12/"}];
        [stations addObject:@{@"name": @"Happy Hardcore", @"url":@"http://u4.happyhardcore.com:7762"}];
        [stations addObject:@{@"name": @"Trance FM", @"url":@"http://www.trance.fm/play/tc192.pls"}];
        [stations addObject:@{@"name": @"Trance FM Classic", @"url":@"http://www.trance.fm/play/classic192.pls"}];
        [stations addObject:@{@"name": @"Trance FM DJ", @"url":@"http://www.trance.fm/play/dj192.pls"}];
        [stations addObject:@{@"name": @"Trance FM Hard", @"url":@"http://www.trance.fm/play/hard192.pls"}];
        [stations addObject:@{@"name": @"Trance FM Progressive", @"url":@"http://www.trance.fm/play/progressive128aac.pls"}];
                [stations addObject:@{@"name": @"Protogon", @"url":@"http://protogon.co.uk:8002"}];
        [stations addObject:@{@"name": @"Dubstep FM", @"url":@"http://www.dubstep.fm/192.pls"}];
        [stations addObject:@{@"name": @"Xtreme Anime Radio", @"url":@"http://174.123.20.140:8010/"}];
        [stations addObject:@{@"name": @"Animeradio.SU", @"url":@"http://animeradio.su:8000"}];
        [stations addObject:@{@"name": @"any-anime.ru", @"url":@"http://radio2.flex.ru:8000/radionami"}];
        [stations addObject:@{@"name": @"JPopSuki", @"url":@"http://213.239.204.252:8000/stream"}];
        [stations addObject:@{@"name": @"KAWAii Radio", @"url":@"http://listen.radionomy.com/kawaiiradio-official.m3u"}];
        [stations addObject:@{@"name": @"MNHTTN", @"url":@"http://mnhttn.net:8000/stream.mp3"}];
        [stations addObject:@{@"name": @"Relax FM", @"url":@"http://217.29.51.162:8000/relaxfm-72k.aac"}];
        [self _write];
    }
    
  // NSLog(@"%@",stations);
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath   {
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    NSDictionary *sdta = stations[indexPath.row];
    MRStation *st = [[MRStation alloc]init];
    st.isDIStation = false;
    st.title = sdta[@"name"];
    st.url = [NSURL URLWithString:sdta[@"url"]];
    st.representingCell = [tableView cellForRowAtIndexPath:indexPath];
    st.representingViewController = self;
    [[MRTuner sharedTuner]playStation:st];
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    [tableView reloadData];
}
- (void) viewWillAppear:(BOOL)animated  {
    [self.tableView reloadData];
    [super viewWillAppear:animated];
}

- (void) _write {
    [[NSUserDefaults standardUserDefaults]setObject:stations forKey:@"stations"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    return stations.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Zirp" forIndexPath:indexPath];
    
    // Configure the cell...
    cell.textLabel.text = stations[indexPath.row][@"name"];
   // cell.accessoryType = ([[[MRTuner sharedTuner]currStation].url.absoluteString isEqualToString:stations[indexPath.row][@"url"] ] ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone);
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [stations removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
        [self _write];
    }
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return NO;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
