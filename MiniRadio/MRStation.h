//
//  MRStation.h
//  MiniRadio
//
//  Created by Akasaka Ryuunosuke on 25/10/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MRStationListViewController.h"
@interface MRStation : NSObject
@property (nonatomic,retain) NSString* title;
@property (nonatomic) bool isDIStation;
@property (nonatomic, retain) NSString* diKey;
@property (nonatomic,retain) NSURL* url;
@property (nonatomic,retain) MRStationListViewController* representingViewController;
@property (nonatomic,retain) UITableViewCell *representingCell;
@end
