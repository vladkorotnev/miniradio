//
//  MRStationListViewController.h
//  MiniRadio
//
//  Created by Akasaka Ryuunosuke on 07/11/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import <UIKit/UIKit.h>
#define ARROW_HEIGHT 8
@interface MRStationListViewController : UITableViewController
- (void) moveArrow:(CGFloat)to;
@end
