//
//  MRKiokuViewController.m
//  MiniRadio
//
//  Created by Akasaka Ryuunosuke on 03/11/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import "MRKiokuViewController.h"
#import "MRKioku.h"
#import "MRTuner.h"
#import "NSObject+AKRObjectHandyExtensions.h"
@interface MRKiokuViewController ()

@end

@implementation MRKiokuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
     self.navigationItem.leftBarButtonItem = self.editButtonItem;
     
    [[NSNotificationCenter defaultCenter]addObserverForName:kMRKiokuAddedNew object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        [self.tableView reloadData]; //2 lazy 2 do shit properly
    }];
   
}
- (IBAction)add:(id)sender {
    [[MRTuner sharedTuner]kiokuWrite];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    return [[MRKioku sharedInstance]readKioku].count;
}

#define TEXTREPLACER_TAG 1001
#define DETAILREPLACER_TAG 1002
#define PROGBAR_TAG 1003
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"kiokuCell" forIndexPath:indexPath];
    
    MarqueeLabel* mlbl = (MarqueeLabel*)[cell findSubviewWithTag:TEXTREPLACER_TAG];
    if (!mlbl) {
        mlbl = [[MarqueeLabel alloc]initWithFrame:CGRectMake(0, 6, cell.frame.size.width-20, 20) duration:3.0 andFadeLength:15];
        [mlbl setHoldScrolling:YES];
        [mlbl setTag:TEXTREPLACER_TAG];
        [mlbl setFont: cell.textLabel.font];
        [mlbl setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
        [cell addSubview:mlbl];
    }
    MarqueeLabel* slbl = (MarqueeLabel*)[cell findSubviewWithTag:DETAILREPLACER_TAG];
    if (!slbl) {
        slbl = [[MarqueeLabel alloc]initWithFrame:CGRectMake(0, 26, cell.frame.size.width-30, 14) duration:3.0 andFadeLength:15];
        [slbl setHoldScrolling:YES];
        [slbl setTag:DETAILREPLACER_TAG];
        [slbl setFont: cell.detailTextLabel.font];
        [slbl setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
        [cell addSubview:slbl];
    }
    // Configure the cell...
    mlbl.text = [[MRKioku sharedInstance]readKioku] [indexPath.row][@"title"];
    slbl.text = [[MRKioku sharedInstance]readKioku] [indexPath.row][@"station"];
    cell.textLabel.text = cell.detailTextLabel.text = @"";
    cell.accessoryType = ([[MRKioku sharedInstance]isKiokuSampleAvailable:indexPath.row] ? UITableViewCellAccessoryDisclosureIndicator : UITableViewCellAccessoryNone);
    return cell;
}


- (void) viewWillAppear:(BOOL)animated {
    [self.tableView reloadData];
    // determine where is kioku located
   
    int kio = -1;
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"hp-one"] isEqualToString:@"kioku"]) kio=1;
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"hp-two"] isEqualToString:@"kioku"]) kio=2;
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"hp-three"] isEqualToString:@"kioku"]) kio=3;
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"hp-twohold"] isEqualToString:@"kioku"]) kio=4;
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"hp-threehold"] isEqualToString:@"kioku"]) kio=5;
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"hp-plusminus"] isEqualToString:@"kioku"]) kio=6;
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"hp-minusplus"] isEqualToString:@"kioku"]) kio=7;
        switch (kio) {
            case 1:
                self.navigationItem.prompt = NSLocalizedString(@"To save track tap headphone button.", @"Kioku hint");
                break;
            case 2:
                self.navigationItem.prompt = NSLocalizedString(@"To save track tap headphone button twice.", @"Kioku hint");
                break;
            case 3:
                self.navigationItem.prompt = NSLocalizedString(@"To save track tap headphone button thrice.", @"Kioku hint");
                break;
            case 4:
                self.navigationItem.prompt = NSLocalizedString(@"To save double-tap & hold headphone button.", @"Kioku hint");
                break;
            case 5:
                self.navigationItem.prompt = NSLocalizedString(@"To save 3-tap & hold headphone button.", @"Kioku hint");
                break;
            case 6:
                self.navigationItem.prompt = NSLocalizedString(@"To save track tap + then - button.", @"Kioku hint");
                break;
            case 7:
                self.navigationItem.prompt = NSLocalizedString(@"To save track tap - then + button.", @"Kioku hint");
                break;
            
            default:
                self.navigationItem.prompt = NSLocalizedString(@"Go to Settings to set Kioku shortcut.", @"Kioku hint");
                break;
        }
    [super viewWillAppear:animated];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex != alertView.cancelButtonIndex) {
        [[MRKioku sharedInstance]playKiokuSample:alertView.tag];
        [alertView show];
        
    }
}
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath    {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    MarqueeLabel* mlbl = (MarqueeLabel*)[cell findSubviewWithTag:TEXTREPLACER_TAG];
    MarqueeLabel* slbl = (MarqueeLabel*)[cell findSubviewWithTag:DETAILREPLACER_TAG];
    if ([[MRKioku sharedInstance]isKiokuSampleAvailable:indexPath.row]) {
        [[MRKioku sharedInstance]playKiokuSample:indexPath.row];
    }
    [mlbl restartLabel]; [slbl restartLabel];
    [tableView deselectRowAtIndexPath:indexPath animated:true];
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [[MRKioku sharedInstance]deleteFromKioku:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationMiddle];
        
    }
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
