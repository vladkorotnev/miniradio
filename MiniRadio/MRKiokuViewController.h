//
//  MRKiokuViewController.h
//  MiniRadio
//
//  Created by Akasaka Ryuunosuke on 03/11/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MarqueeLabel.h"
@interface MRKiokuViewController : UITableViewController <UIAlertViewDelegate>

@end
