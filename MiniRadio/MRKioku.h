//
//  MRKioku.h
//  MiniRadio
//
//  Created by Akasaka Ryuunosuke on 03/11/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#define kMRKiokuAddedNew @"MRKiokuAddedNew"
#define kMRKiokuRemoved @"MRKiokuRemoved"
@interface MRKioku : NSObject<AVAudioPlayerDelegate> {
    NSMutableArray* kiokudata;
}
+ (MRKioku*)sharedInstance;
- (void) addToKioku:(NSString*)title ofStation:(NSString*)station audioSampleFile:(NSString*)name;
- (NSArray*)readKioku;
- (void) deleteFromKioku:(NSInteger)index;
- (bool) isKiokuSampleAvailable: (NSInteger) index;
- (void) playKiokuSample: (NSInteger)index;
@end
