//
//  MRStationListViewController.m
//  MiniRadio
//
//  Created by Akasaka Ryuunosuke on 07/11/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import "MRStationListViewController.h"
#import "NSObject+AKRObjectHandyExtensions.h"
@interface MRStationListViewController ()
@property (nonatomic) UIView* _arrow;
@end

@implementation MRStationListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self._arrow =   [UIView new];
    [self._arrow setFrame:CGRectMake(0, -9, self.view.frame.size.width, ARROW_HEIGHT)];
    [self._arrow setAlpha:0.5];
    [self._arrow setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [self._arrow setBackgroundColor:[UIColor redColor]];
    [self._arrow.layer setShadowColor:[[UIColor blackColor]CGColor]];
    [self._arrow.layer setShadowOffset:CGSizeMake(1, 1)];
    [self._arrow.layer setShadowOpacity:0.4];
    [self._arrow setParallaxDirectionConstraint:NGAParallaxDirectionConstraintVertical];
    [self._arrow setParallaxIntensity:12];
    [self.tableView addSubview:self._arrow];
    [self.tableView setParallaxIntensity:-6];
}

- (void) moveArrow: (CGFloat)to {
    [UIView animateWithDuration:0.4
                          delay:0.1
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self._arrow setFrame:CGRectMake(0, to+2, self.view.frame.size.width, 4)];
                     }
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:0.2
                                               delay:0.1
                                             options:UIViewAnimationOptionCurveEaseIn
                                          animations:^{
                                              [self._arrow setFrame:CGRectMake(0, to-4, self.view.frame.size.width, 4)];
                                          }
                                          completion:^(BOOL finished){
                                              [UIView animateWithDuration:0.2
                                                                    delay:0.1
                                                                  options:UIViewAnimationOptionCurveEaseOut
                                                               animations:^{
                                                                   [self._arrow setFrame:CGRectMake(0, to, self.view.frame.size.width, 4)];
                                                               }
                                                               completion:^(BOOL finished){
                                                               }];
                                          }];
                     }];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath   {
    UITableViewCell*cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell) {
        [self moveArrow: (cell.frame.origin.y + cell.frame.size.height / 2 - ARROW_HEIGHT/2)];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
