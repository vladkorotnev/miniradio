//
//  PTDigitallyImportedTuner.m
//  PortaTune
//
//  Created by Akasaka Ryuunosuke on 06/12/13.
//  Copyright (c) 2013 Akasaka Ryuunosuke. All rights reserved.
//

#import "PTDigitallyImportedTuner.h"

@implementation PTDigitallyImportedTuner
+ (void) getStationListForCallback:(void (^)(NSArray *))success {
    NSLog(@"DI FM Engaged");
    MKNetworkOperation* recvOp = [[MKNetworkOperation alloc]initWithURLString:[NSString stringWithFormat:@"http://listen.di.fm/%@",DI_TIER] params:nil httpMethod:@"GET"];
    [recvOp addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        NSArray* stts = [completedOperation.responseJSON sortedArrayWithOptions:0 usingComparator:^NSComparisonResult(NSDictionary* obj1, NSDictionary* obj2) {
            NSArray* temp = @[obj1[@"name"], obj2[@"name"]];
            NSArray* tsort = [temp sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
            if ([tsort[0] isEqualToString:temp[1]]) {
                return NSOrderedDescending;
            } else if ([tsort[0] isEqualToString:temp[1]]) {
                return NSOrderedAscending;
            } else {
                return NSOrderedSame;
            }
        }];
        
        success(stts);
        
    } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
        [[[UIAlertView alloc]initWithTitle:@"Error" message:@"Error when connecting to DI.fm" delegate:self cancelButtonTitle:@"Oh well" otherButtonTitles: nil]show];
    }];
    [recvOp start];
}
+ (void) resolveStationURLForStation:(NSString*)station callback:(void (^)(NSURL *))success {
    NSLog(@"DI FM Engaged");
    MKNetworkOperation* recvOp = [[MKNetworkOperation alloc]initWithURLString:[NSString stringWithFormat:@"http://listen.di.fm/%@/%@",DI_TIER,station] params:nil httpMethod:@"GET"];
    [recvOp addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        if (completedOperation.responseJSON[0]) {
            NSURL* surl = [NSURL URLWithString:completedOperation.responseJSON[0]];
            if(success) success(surl);
        }
    } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
        [[[UIAlertView alloc]initWithTitle:@"Error" message:@"Error when connecting to DI.fm" delegate:self cancelButtonTitle:@"Oh well" otherButtonTitles: nil]show];
    }];
    [recvOp start];
}
@end
