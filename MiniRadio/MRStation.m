//
//  MRStation.m
//  MiniRadio
//
//  Created by Akasaka Ryuunosuke on 25/10/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import "MRStation.h"

@implementation MRStation
- (BOOL)isEqual:(id)object {
    if (![object isKindOfClass:[MRStation class]]) return FALSE;
    MRStation* ms = object;
    if (ms.isDIStation) {
        if(!self.isDIStation) return FALSE;
        if(![self.diKey isEqualToString:ms.diKey]) return FALSE;
    } else {
        if(![self.url isEqual:ms.url]) return FALSE;
    }
    return TRUE;
}
@end
