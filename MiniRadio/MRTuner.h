//
//  MRTuner.h
//  MiniRadio
//
//  Created by Akasaka Ryuunosuke on 25/10/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import "MRStation.h"
#import "FSAudioController.h"
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
@interface MRTuner : NSObject <AVSpeechSynthesizerDelegate> {
    FSAudioController *_controller;
    bool _paused;
    bool _recording;
    bool _maySafelyAnnounce;
    float _volume;
    AVSpeechSynthesizer *synth;
    bool _retried;
}
+ (MRTuner*)sharedTuner;
@property (nonatomic) MRStation* currStation;
@property (nonatomic,strong) FSAudioController *audioController;
- (void) playStation: (MRStation*)s;
- (void) mannerMode;
- (void) kiokuWrite;
- (void) speakInfo;
- (void) _playPause;
- (BOOL) playing;
- (void) _playUrl: (NSURL* )url;
- (void) _return;
- (void) recordSampleFor:(NSTimeInterval)seconds toFileName:(NSString*)filename;
@end
