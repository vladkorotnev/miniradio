//
//  MRHeadsetButtonControl.h
//  MiniRadio
//
//  Created by Akasaka Ryuunosuke on 05/11/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MediaPlayer/MediaPlayer.h>
@interface MRHeadsetButtonControl : NSObject {
    MPVolumeView *_volview;
    float _prevVol, _origVol;
    int _volChangeCount;
    NSDate *_lastVolChange;
    
}
+ (MRHeadsetButtonControl*)sharedInstance;
- (void)remoteControlReceivedWithEvent:(UIEvent *)receivedEvent;
@end
