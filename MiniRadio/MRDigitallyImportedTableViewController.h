//
//  MRDigitallyImportedTableViewController.h
//  MiniRadio
//
//  Created by Akasaka Ryuunosuke on 25/10/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PTDigitallyImportedTuner.h"
#import "MRStation.h"
#import "MRStationListViewController.h"
@interface MRDigitallyImportedTableViewController : MRStationListViewController

@end
