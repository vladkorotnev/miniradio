//
//  NSObject+AKRObjectHandyExtensions.h
//  PDBReader
//
//  Created by Akasaka Ryuunosuke on 02/11/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger, NGAParallaxDirectionConstraint) {
    NGAParallaxDirectionConstraintAll = 0,
    NGAParallaxDirectionConstraintHorizontal,
    NGAParallaxDirectionConstraintVertical
};


@interface UIView (NGAParallaxMotion)

// Positive values make the view appear to be above the surface
// Negative values are below.
// The unit is in points
@property (nonatomic) CGFloat parallaxIntensity;

// When filled up, will restrict the parallax to a certain direction only
// Default to NGAParallaxDirectionConstraintAll
@property (nonatomic) NGAParallaxDirectionConstraint parallaxDirectionConstraint;

@end
@interface NSString (AKRStringAdditions)

- (BOOL)containsString:(NSString *)string;
- (BOOL)containsString:(NSString *)string
               options:(NSStringCompareOptions)options;

@end
@interface NSObject (AKRObjectHandyExtensions)
- (void) callIfImplemented:(SEL)selector;
- (void) callIfImplemented:(SEL)selector withObject:(id)object;
@end

@interface UIView (AKRViewExtensions)
- (UIView*) findSubviewWithTag: (NSInteger)tag;
@end