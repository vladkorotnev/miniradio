//
//  MRMainViewController.m
//  MiniRadio
//
//  Created by Akasaka Ryuunosuke on 07/11/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import "MRMainViewController.h"
#import "EAIntroView/EAIntroView.h"
#import "NSObject+AKRObjectHandyExtensions.h"
@interface MRMainViewController ()

@end

@implementation MRMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (![[NSUserDefaults standardUserDefaults]boolForKey:@"did-show-intro"]) {
        EAIntroPage* welcome = [EAIntroPage page];
        welcome.title = NSLocalizedString(@"Welcome.", @"Welcome pages.");
        welcome.desc = NSLocalizedString(@"Welcome to MRadio by AkR Soft, the most simplistic yet powerful portable radio application.\nTap to move on.", @"Welcome pages.");
        welcome.titleFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:48];
        welcome.titlePositionY = [UIScreen mainScreen].bounds.size.height/2 + [welcome.title sizeWithFont:welcome.titleFont].height/2;
        welcome.descPositionY = welcome.titlePositionY - [welcome.title sizeWithFont:welcome.titleFont].height ;
        welcome.bgImage = [UIImage imageNamed:@"bkg_lgo"];
        
        EAIntroPage* stations = [EAIntroPage page];
        stations.title = NSLocalizedString(@"Tune in!", @"Welcome pages.");
        stations.desc = NSLocalizedString(@"Type in your own stations by URL on the My Stations tab, then tap to tune in. The red arrow indicates what wave you are currently on.", @"Welcome pages.");
        stations.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Intro_Stations"]];
        stations.bgImage = [UIImage imageNamed:@"bg1"];
        
        EAIntroPage* di = [EAIntroPage page];
        di.title = NSLocalizedString(@"Digitally Imported", @"Welcome pages.");
        di.desc = NSLocalizedString(@"DI.fm is the leading online radio provider, a must for every music addict, and we're proud to have it supported in our app.", @"Welcome pages.");
        di.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Intro_DI"]];
        di.bgImage = [UIImage imageNamed:@"bg3"];
        
        EAIntroPage* kioku = [EAIntroPage page];
        kioku.title = NSLocalizedString(@"Kioku Track Memory", @"Welcome pages.");
        kioku.desc = NSLocalizedString(@"A unique feature to MRadio.\nIt allows you to save titles of tracks you liked while on the go, along with a small audio sample. More than that — without taking your phone out of your pocket!", @"Welcome pages.");
        kioku.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Intro_Kioku"]];
        kioku.bgImage = [UIImage imageNamed:@"bg4"];
        
        EAIntroPage* np = [EAIntroPage page];
        np.title = NSLocalizedString(@"Now Playing", @"Welcome pages.");
        
        np.titleIconPositionY = 120;
        np.desc = NSLocalizedString(@"MRadio integrates with the iOS Now Playing feature on lockscreen and Control Center so that you know what you're listening to at any time.", @"Welcome pages.");
        np.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Intro_NP"]];
        np.bgImage = [UIImage imageNamed:@"bg1"];
        
        
        EAIntroPage* hp = [EAIntroPage page];
        hp.title = NSLocalizedString(@"Headphone Controls", @"Welcome pages.");
        hp.titlePositionY = [UIScreen mainScreen].bounds.size.height/2 + [hp.title sizeWithFont:hp.titleFont].height/2 - 15;
        hp.descPositionY = hp.titlePositionY - [hp.title sizeWithFont:hp.titleFont].height ;
        hp.titleIconPositionY = 240;
        if ([UIScreen mainScreen].bounds.size.height == 480 && [UIScreen mainScreen].bounds.size.width==320) {
            NSLog(@"4S screen");
            hp.titleColor = [UIColor blackColor];
        }
        hp.desc = NSLocalizedString(@"If your headset has remote control buttons, you can use them to control MRadio. And set up the way to control it in Settings, too!", @"Welcome pages.");
        hp.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Heddofon"]];
        hp.bgImage = [UIImage imageNamed:@"bg2"];
        
        EAIntroPage* conf = [EAIntroPage page];
        conf.title = NSLocalizedString(@"Flexible Configutation", @"Welcome pages.");
         conf.titleIconPositionY = 20;
        conf.desc = NSLocalizedString(@"If you go to the Settings app and pick MiniRadio from the menu, you can adjust almost anything, such as headset behaviour, QSilence level, speech volume, track announcements, and a lot more.", @"Welcome pages.");
        conf.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Intro_Config"]];
        conf.bgImage = [UIImage imageNamed:@"bg3"];
        
        EAIntroPage* finita = [EAIntroPage page];
        finita.title = NSLocalizedString(@"Enjoy!", @"Welcome pages.");
        finita.titleFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:32];
        finita.titlePositionY = [UIScreen mainScreen].bounds.size.height/2 + [finita.title sizeWithFont:finita.titleFont].height/2;
        finita.descPositionY = finita.titlePositionY - [finita.title sizeWithFont:finita.titleFont].height ;
        finita.desc = NSLocalizedString(@"I hope you will like my app :)\n~Akasaka Ryuunosuke\nGreetings to: alsalisa, nyanya98, dmgcat, iT0ny,", @"Welcome pages.");
        finita.bgImage = [UIImage imageNamed:@"bkg_lgo"];
        
        EAIntroView* iv = [[EAIntroView alloc]initWithFrame:self.view.bounds andPages:@[welcome, stations,kioku,di,np,hp,conf,finita]];
        [iv setUseMotionEffects:true];
        [iv setMotionEffectsRelativeValue:4.0f];
        iv.tapToNext = YES;
        [iv showInView:self.view animateDuration:0];
        [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"did-show-intro"];
    }
}
- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSLog(@"Did appear sas");
    
}
- (void) viewWillAppear:(BOOL)animated {
   
     [super viewWillAppear:animated];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
