//
//  MRTuner.m
//  MiniRadio
//
//  Created by Akasaka Ryuunosuke on 25/10/14.
//  Copyright (c) 2014 Akasaka Ryuunosuke. All rights reserved.
//

#import "MRTuner.h"
#import "FSAudioStream.h"
#import "FSPlaylistItem.h"
#import "PTDigitallyImportedTuner.h"
#import "MRKioku.h"
@implementation MRTuner
@synthesize audioController=_audioController;
+ (MRTuner *)sharedTuner
{
    static MRTuner *sharedInstance = nil;
    if (sharedInstance == nil)
    {
        sharedInstance = [[self alloc] init];
        sharedInstance.audioController   = [[FSAudioController alloc] init];;
        [sharedInstance _prepAC];
    }
    return sharedInstance;
}
- (AVSpeechSynthesisVoice*) _getVoiceForString:(NSString*)sample {
    NSString* lcode = [[NSUserDefaults standardUserDefaults]objectForKey:@"voice-lang"];
    
    if ((!lcode || [lcode isEqualToString:@"sysrq"]) && (sample && ![sample isEqualToString:@""])){
        NSArray *tagschemes = [NSArray arrayWithObjects:NSLinguisticTagSchemeLanguage, nil];
        NSLinguisticTagger *tagger = [[NSLinguisticTagger alloc] initWithTagSchemes:tagschemes options:0];
        [tagger setString:sample];
        NSString *language = [tagger tagAtIndex:0 scheme:NSLinguisticTagSchemeLanguage tokenRange:NULL sentenceRange:NULL];
        NSLog(@"Autodetect lang: %@", language);
        if (language) {
            for (AVSpeechSynthesisVoice*lvo in [AVSpeechSynthesisVoice speechVoices]) {
                if ([lvo.language hasPrefix:language] || [lvo.language isEqualToString:language]) {
                    lcode = lvo.language;
                }
            }
        }
    }
    if (!lcode || [lcode isEqualToString:@"sysrq"]) {
        lcode = [AVSpeechSynthesisVoice currentLanguageCode];
    }
    return [AVSpeechSynthesisVoice voiceWithLanguage:lcode];
}
- (AVSpeechUtterance*) _mkNPUtterance {
    NSDictionary *np = [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo;
    
    NSString*msg = [NSString stringWithFormat:@"%@.", np[MPMediaItemPropertyTitle]];
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"append-station"])
        msg = [msg stringByAppendingFormat:@" %@.",np[MPMediaItemPropertyArtist]];
    AVSpeechUtterance *uttr = [AVSpeechUtterance
                               speechUtteranceWithString:msg];
    uttr.voice = [self _getVoiceForString:msg];
    uttr.rate = AVSpeechUtteranceMinimumSpeechRate;
    return uttr;
}
- (AVSpeechUtterance*) _mkKiokuUtterance {
    NSDictionary *np = [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo;
    NSString*msg = [NSString stringWithFormat:@"Track memory: %@.", np[MPMediaItemPropertyTitle]];
    AVSpeechUtterance *uttr = [AVSpeechUtterance
                               speechUtteranceWithString:msg];
    uttr.voice = [self _getVoiceForString:msg];
    uttr.rate = AVSpeechUtteranceDefaultSpeechRate;
    return uttr;
}

- (void) kiokuWrite {
    NSLog(@"Kioku write");
    if(_paused) return;
    NSDictionary *np = [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo;
    NSString*fileN = @"[NOFILE]";
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"kioku-notify"]) {
        [synth speakUtterance:[self _mkKiokuUtterance]];
        NSLog(@"Kioku notify enabled");
    }
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"kioku-sample"]) {
        fileN = [NSString stringWithFormat:@"%@-%0.0f.%@",[np[MPMediaItemPropertyTitle] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding], [[NSDate date]timeIntervalSince1970], self.audioController.stream.suggestedFileExtension];
        [self recordSampleFor:[[NSUserDefaults standardUserDefaults]doubleForKey:@"kioku-length"] toFileName:fileN];
    }
    [[MRKioku sharedInstance]addToKioku:np[MPMediaItemPropertyTitle] ofStation:np[MPMediaItemPropertyArtist] audioSampleFile:fileN];
}
- (void) recordSampleFor:(NSTimeInterval)seconds toFileName:(NSString*)filename {
    [self _startRecording:filename];
    [self performSelector:@selector(_stopRecording) withObject:nil afterDelay:seconds];
}
- (void) _startRecording:(NSString*)filename {
    if(_recording) {
        NSLog(@"Warning: tried to record while recording, ignoring...");
        return;
    }
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths[0];
    NSString *f = [documentsDirectory stringByAppendingPathComponent:filename];
    NSURL *url = [NSURL fileURLWithPath:f];
    NSLog(@"Recording to %@...",f);
    self.audioController.stream.outputFile = url;
    _recording = true;
}
- (void) _stopRecording {
    if(!_recording) return;
    _recording = false;
    NSLog(@"End recording...");
    self.audioController.stream.outputFile = nil;
}
- (void) _playPause {
    if(!self.currStation)return;
    NSLog(@"Play-pause...");
    if (!_paused) {
        [self.audioController stop];
    } else {
        [self.audioController play];
    }
}
- (BOOL) playing { return !_paused; }
- (void) speakInfo {
    // Speak track name
    if(_paused)return;
    if (synth.speaking) {
        [synth stopSpeakingAtBoundary:AVSpeechBoundaryImmediate];
        NSLog(@"Already speaking!");
        [self.audioController setVolume:_volume];
    } else
        [synth speakUtterance:[self _mkNPUtterance]];

}

- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didStartSpeechUtterance:(AVSpeechUtterance *)utterance {
    [self.audioController setVolume:(_volume * [[NSUserDefaults standardUserDefaults]floatForKey:@"speech-muting"])];
}
- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didFinishSpeechUtterance:(AVSpeechUtterance *)utterance {
    [self.audioController setVolume:_volume];
}
- (void) _prepAC {
    
    synth = [[AVSpeechSynthesizer alloc] init];
    [synth setDelegate:self];
    self.audioController.stream.onStateChange = ^(FSAudioStreamState state) {
        switch (state) {
            case kFsAudioStreamRetrievingURL:
                [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
                
                break;
                
            case kFsAudioStreamStopped:
                _paused = true;
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                break;
                
            case kFsAudioStreamBuffering:
                _paused = true;
                [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
                break;
                
                
            case kFsAudioStreamPlaying:
                _paused =false;
                _maySafelyAnnounce = true;
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                
                break;
                
            case kFsAudioStreamFailed:
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                
                break;
            default:
                break;
        }
    };
    
    self.audioController.stream.onFailure = ^(FSAudioStreamError error) {
        NSString *errorDescription;
        
        switch (error) {
            case kFsAudioStreamErrorOpen:
                errorDescription = @"Cannot open the audio stream";
                break;
            case kFsAudioStreamErrorStreamParse:
                errorDescription = @"Cannot read the audio stream";
                break;
            case kFsAudioStreamErrorNetwork:
                errorDescription = @"Network failed: cannot play the audio stream";
                break;
            case kFsAudioStreamErrorUnsupportedFormat:
                errorDescription = @"Unsupported format";
                break;
            case kFsAudioStreamErrorStreamBouncing:
                errorDescription = @"Network failed: cannot get enough data to play";
                break;
            default:
                errorDescription = @"Unknown error occurred";
                break;
        }
        UIAlertView*a = [[UIAlertView alloc]initWithTitle:@"Error" message:[errorDescription stringByAppendingString:(_retried?@"":@"\nRetry in 2sec...")] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [a show];
        if(!_retried && self.currStation) {
            [self performSelector:@selector(_playStation:) withObject:self.currStation afterDelay:2.0];
            
            [a performSelector:@selector(setMessage:) withObject:errorDescription  afterDelay:2.0];
            _retried = true;
        } else _retried = false;
    };
    
    self.audioController.stream.onMetaDataAvailable = ^(NSDictionary *md) {
        //NSMutableString *streamInfo = [[NSMutableString alloc] init];
        NSMutableDictionary *metaData = [md mutableCopy];
        if(!metaData[@"IcecastStationName"] && self.currStation.title) metaData[@"IcecastStationName"] = self.currStation.title;
        
        NSMutableDictionary *songInfo = [[NSMutableDictionary alloc] init];
        NSLog(@"meta %@", metaData);
        if (metaData[@"MPMediaItemPropertyTitle"]) {
            songInfo[MPMediaItemPropertyTitle] = metaData[@"MPMediaItemPropertyTitle"];
        } else if (metaData[@"StreamTitle"]) {
            songInfo[MPMediaItemPropertyTitle] = metaData[@"StreamTitle"];
        }
       if(metaData[@"IcecastStationName"]) songInfo[MPMediaItemPropertyArtist] =metaData[@"IcecastStationName"];
        
        if (metaData[@"MPMediaItemPropertyArtist"]) {
            songInfo[MPMediaItemPropertyArtist] = metaData[@"MPMediaItemPropertyArtist"];
            if(metaData[@"IcecastStationName"]) songInfo[MPMediaItemPropertyAlbumTitle] =metaData[@"IcecastStationName"];
            
        }
        
        [[MPNowPlayingInfoCenter defaultCenter] setNowPlayingInfo:songInfo];
        
        if ([[NSUserDefaults standardUserDefaults]boolForKey:@"track-announce"] && _maySafelyAnnounce)
            [self speakInfo];
        
        
        
    };
    _paused = true;
    _volume = 1.0;
    [self.audioController setVolume:_volume];
}

- (void) mannerMode {
    if(_paused) return;
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(_mannerModeDidEnd) object:nil];
    _volume = [[NSUserDefaults standardUserDefaults]floatForKey:@"qsilence-level"];
    [self.audioController setVolume:_volume];
    [self performSelector:@selector(_mannerModeDidEnd) withObject:nil afterDelay:[[NSUserDefaults standardUserDefaults]doubleForKey:@"qsilence-length"]];
    
}
- (void) _mannerModeDidEnd {
    _volume = 1.0;
    [self.audioController setVolume:_volume];
}
- (void) playStation: (MRStation*)s {
    if([s isEqual:self.currStation])return;
    [self _playStation:s];
}
- (void) _playStation: (MRStation*)s {
    _maySafelyAnnounce = false;
    if (self.currStation) {
        if(self.currStation.representingViewController != s.representingViewController)
            [self.currStation.representingViewController moveArrow:(-1*ARROW_HEIGHT)];
        
        [s.representingViewController moveArrow:(s.representingCell.frame.origin.y + s.representingCell.frame.size.height / 2 - ARROW_HEIGHT/2)];
    }
    self.currStation = s;
    if (s.isDIStation) {
        [PTDigitallyImportedTuner resolveStationURLForStation:s.diKey callback:^(NSURL *sas) {
            [self.audioController playFromURL:sas];
        }];
    } else {
        [self.audioController playFromURL:s.url];
    }
}
- (void) _return {
    [self _playStation:self.currStation];
}
- (void) _playUrl: (NSURL* )url {

    self.currStation = nil;
    [self.audioController playFromURL:url];
}
@end
